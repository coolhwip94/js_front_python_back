from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
# create FastAPI instance
# this is referenced command used to run app
# uvicorn main:app --reload
app = FastAPI()

origins = [
    "http://127.0.0.1:5500",
]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

@app.get("/")
async def root():
    return {"message": "Hello World"}
